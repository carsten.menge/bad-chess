﻿using Assets.Scripts.Chess;
using Assets.Scripts.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class PlayerHandler : MonoBehaviour
    {
        public Board board;
        public UIBoard uiBoard;
        public enum InputState
        {
            Selecting,
            Dragging
        }
        Piece piece;
        SpriteRenderer pieceRenderer;
        InputState currentState;

        public void Update()
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0) && currentState == InputState.Selecting)
            {
                int rank = (int) Math.Floor(mousePos.x + 0.5);
                int file = (int) Math.Floor(mousePos.y + 0.5);
                
                if (board.Squares[BoardUtility.CoordinatesToIndex(file, rank)] == 0) return;

                piece = new Piece(board.Squares[BoardUtility.CoordinatesToIndex(file, rank)]);

                // remove the piece from the board
                board.Squares[BoardUtility.CoordinatesToIndex(file, rank)] = 0;

                // add a sprite
                pieceRenderer = new GameObject().AddComponent<SpriteRenderer>();
                pieceRenderer.transform.parent = this.transform;
                pieceRenderer.transform.position = mousePos;
                pieceRenderer.transform.localScale = Vector3.one * 100 / (2000 / 6f);
                pieceRenderer.sprite = uiBoard.pieceSpriteHandler.GetPieceSprite(piece.Id);

                uiBoard.UpdateBoard();
                currentState = InputState.Dragging;
            }
            else if (Input.GetMouseButtonDown(0) && currentState == InputState.Dragging)
            {
                int rank = (int)Math.Floor(mousePos.x + 0.5);
                int file = (int)Math.Floor(mousePos.y + 0.5);

                // put it on the board
                board.Squares[BoardUtility.CoordinatesToIndex(file, rank)] = piece.Id;

                // remove the sprite
                Destroy(pieceRenderer.gameObject);
                pieceRenderer = null;

                uiBoard.UpdateBoard();
                currentState = InputState.Selecting;
            }
            else if (currentState == InputState.Dragging)
            {
                pieceRenderer.transform.position = mousePos;
            }
        }
    }
}
