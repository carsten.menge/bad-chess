﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Chess;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class GameHandler : MonoBehaviour
    {
        public Board board;
        public UIBoard uiBoard;
        public PlayerHandler player;


        public void Start()
        {
            board = new Board();
            player = gameObject.AddComponent<PlayerHandler>();
            player.board = board;
            player.uiBoard = uiBoard;
            uiBoard.InitializeBoard(board);
        }
    }
}
