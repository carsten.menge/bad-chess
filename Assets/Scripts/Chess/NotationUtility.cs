﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Chess
{
    public static class NotationUtility
    {
        public static int[] BoardFromFENString(string fen)
        {
            string[] splitFEN = fen.Split(null);
            int[] board = new int[64];

            if (splitFEN.Length < 1) throw new InvalidNotationException($"Invalid FEN: not a valid FEN string: '{fen}'");

            // squares
            string[] ranks = splitFEN[0].Split('/');
            if (ranks.Length != 8) throw new InvalidNotationException($"Invalid FEN: position does not contain exactly 8 files");

            // we have to work kinda backwards, because the 0,0 field is a8 and 7,7 is h1
            int index = 63;

            foreach (var rank in ranks)
            {
                int validation = 0;
                var rankStr = ReverseString(rank);

                foreach (var c in rankStr)
                {
                    if (48 < c && c < 57)
                    {
                        index = index - (c - 48);
                        validation = validation + (c - 48);
                    } else
                    {
                        try { board[index] = CharToPiece(c); index--; validation++; }
                        catch (Exception e) { throw new InvalidNotationException(e.Message); }
                    }
                }

                if (validation != 8) throw new InvalidNotationException($"Invalid FEN rank length: {rank}");
            }

            return board;
        }

        public static bool WhiteToMoveFromFEN(string fen)
        {
            string[] splitFEN = fen.Split(null);
            if (splitFEN.Length < 2) return true;
            else
            {
                if (splitFEN[1] == "w") return true;
                else if (splitFEN[1] == "b") return false;
                else throw new InvalidNotationException($"Invalid FEN: '{splitFEN[1]}' is no valid move right value");
            }
        }

        public static uint CastlingRightsFromFEN(string fen)
        {
            string[] splitFEN = fen.Split(null);
            uint castlingRights = 0b0000;

            if (splitFEN.Length < 3) return castlingRights;

            string castlingString;
            castlingString = splitFEN[2];

            if (castlingString.Contains('K')) castlingRights = castlingRights | Board.BlackCanCastleShortMask;
            if (castlingString.Contains('Q')) castlingRights = castlingRights | Board.BlackCanCastleLongMask;
            if (castlingString.Contains('k')) castlingRights = castlingRights | Board.WhiteCanCastleShortMask;
            if (castlingString.Contains('q')) castlingRights = castlingRights | Board.WhiteCanCastleLongMask;

            return castlingRights;
        }

        public static int EnPassantIndexFromFEN(string fen)
        {
            string[] splitFEN = fen.Split(null);
            if (splitFEN.Length < 4 || splitFEN[3].Contains('-')) return -1;
            else return CoordinatesToIndex(splitFEN[3]);
        }

        public static int CoordinatesToIndex(string notationString)
        {
            if (notationString.Length != 2) throw new InvalidNotationException($"Invalid coordinates: {notationString}");

            char rankChar = notationString[0];
            char fileChar = notationString[1];

            Dictionary<char, int> legalFiles = new Dictionary<char, int>()
                { { 'a', 0 }, { 'b', 1 }, { 'c', 2 }, { 'd', 3 }, {'e', 4 }, {'f', 5 }, {'g', 6 }, {'h', 7 },
                  { 'A', 0 }, { 'B', 1 }, { 'C', 2 }, { 'D', 3 }, {'E', 4}, {'F', 5 }, {'G', 6 }, {'H', 7 } };
            Dictionary<char, int> legalRanks = new Dictionary<char, int>()
                { { '1', 0 }, { '2', 1 }, { '3', 2 }, { '4', 3 }, {'5', 4 }, {'6', 5 }, {'7', 6 }, {'8', 7 } };

            if (!legalFiles.ContainsKey(fileChar) || !legalRanks.ContainsKey(rankChar)) throw new InvalidNotationException($"Invalid coordinates: {notationString}");

            return legalFiles[fileChar] * legalRanks[rankChar];
        }

        public static string GetCoordinateString(int file, int rank)
        {
            Dictionary<int, char> files = new Dictionary<int, char>()
                { { 0, 'a' }, { 1, 'b' }, { 2, 'c' }, { 3, 'd' }, { 4, 'e' }, { 5, 'f' }, { 6, 'g' }, { 7, 'h' }, };
            Dictionary<int, char> ranks = new Dictionary<int, char>()
                { { 0, '1' }, { 1, '2' }, { 2, '3' }, { 3, '4' }, { 4, '5' }, { 5, '6' }, { 6, '7' }, { 7, '8' } };

            return new string(new char[2] { files[file], ranks[rank] });
        }

        private static int CharToPiece(char c)
        {
            switch (c)
            {
                case 'k': return Piece.Black + Piece.King;
                case 'q': return Piece.Black + Piece.Queen;
                case 'r': return Piece.Black + Piece.Rook;
                case 'b': return Piece.Black + Piece.Bishop;
                case 'n': return Piece.Black + Piece.Knight;
                case 'p': return Piece.Black + Piece.Pawn;
                case 'K': return Piece.White + Piece.King;
                case 'Q': return Piece.White + Piece.Queen;
                case 'R': return Piece.White + Piece.Rook;
                case 'B': return Piece.White + Piece.Bishop;
                case 'N': return Piece.White + Piece.Knight;
                case 'P': return Piece.White + Piece.Pawn;
                default: throw new InvalidNotationException($"Invalid FEN: '{c}' is not a valid piece");
            }
        }

        private static string ReverseString(string str)
        {
            char[] cArr = str.ToCharArray();
            Array.Reverse(cArr);
            return new string(cArr);
        }
    }
}
