﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Chess
{
    // should just use bitmasks, but I don't want to just copy everything :P
    public class Piece
    {
        public const int None = 0;

        public const int King = 1;
        public const int Queen = 2;
        public const int Rook = 3;
        public const int Bishop = 4;
        public const int Knight = 5;
        public const int Pawn = 6;

        public const int White = 8;
        public const int Black = 16;

        public int Id { get; }

        public Piece(int colourPlusType)
        {
            Id = colourPlusType;
        }

        public Piece (int colour, int type)
        {
            Id = colour + type;
        }

        public bool IsWhite
        {
            get
            {
                return Id < Piece.Black;
            }
        }

        public int Colour
        {
            get
            {
                return (Id / 8) * 8;
            }
        }

        public int Type
        {
            get
            {
                return Id % 8;
            }
        }

        public override string ToString()
        {
            if (Id == 0) return "no piece";
            string res = "";
            if (IsWhite) res = res + "white";
            else res = res + "black";

            switch (Type)
            {
                case King: res = res + " king"; break;
                case Rook: res = res + " rook"; break;
                case Queen: res = res + " queen"; break;
                case Bishop: res = res + " bishop"; break;
                case Knight: res = res + " knight"; break;
                case Pawn: res = res + " pawn"; break;
                default: res = "invalid piece"; break;
            }

            return res;
        }
    }
}
