﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Chess
{
    public class InvalidNotationException : Exception
    {
        public InvalidNotationException(string msg) : base(msg)
        {
            
        }
    }
}
