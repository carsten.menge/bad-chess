﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Chess
{
    public static class BoardUtility
    {
        public static Tuple<int, int> IndexToCoordinate(int index)
        {
            int file = index % 8;
            int rank = index / 8;
            return new Tuple<int, int>(file, rank);
        }

        public static int CoordinatesToIndex(int file, int rank)
        {
            return file * 8 + rank;
        }

        public static int CoordinatesToIndex(Tuple<int, int> coords)
        {
            return CoordinatesToIndex(coords.Item1, coords.Item2);
        }
    }
}
