using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Chess
{
    public class Board 
    {
        private string startingFEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

        public int[] Squares;
        public bool WhiteToMove;
        public uint CastlingRights;
        public int PossibleEnPassantSquare;

        public const uint WhiteCanCastleShortMask = 0b1000;
        public const uint WhiteCanCastleLongMask = 0b0100;
        public const uint BlackCanCastleShortMask = 0b0010;
        public const uint BlackCanCastleLongMask = 0b0001;

        public bool WhiteCanCastleShort { get { return (CastlingRights & WhiteCanCastleShortMask) == WhiteCanCastleShortMask; } }
        public bool WhiteCanCastleLong { get { return (CastlingRights & WhiteCanCastleLongMask) == WhiteCanCastleLongMask; } }
        public bool BlackCanCastleShort { get { return (CastlingRights & BlackCanCastleShortMask) == BlackCanCastleShortMask; } }
        public bool BlackCanCastleLong { get { return (CastlingRights & BlackCanCastleLongMask) == BlackCanCastleLongMask; } }

        public Board()
        {
            try { SetFromFEN(startingFEN); }
            catch (Exception e) { throw e; }
        }

        public Board(string fen)
        {
            try { SetFromFEN(fen); }
            catch (Exception e) { throw e; }
        }

        private void SetFromFEN(string fen)
        {
            Squares = NotationUtility.BoardFromFENString(fen);
            WhiteToMove = NotationUtility.WhiteToMoveFromFEN(fen);
            CastlingRights = NotationUtility.CastlingRightsFromFEN(fen);
            PossibleEnPassantSquare = NotationUtility.EnPassantIndexFromFEN(fen);
        }
    }
}

