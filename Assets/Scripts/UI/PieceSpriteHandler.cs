﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Chess;

namespace Assets.Scripts.UI
{
    [CreateAssetMenu(menuName = "Piece Sprites")]
    public class PieceSpriteHandler : ScriptableObject
    {
        public Sprite whiteKing;
        public Sprite whiteQueen;
        public Sprite whiteRook;
        public Sprite whiteBishop;
        public Sprite whiteKnight;
        public Sprite whitePawn;

        public Sprite blackKing;
        public Sprite blackQueen;
        public Sprite blackRook;
        public Sprite blackBishop;
        public Sprite blackKnight;
        public Sprite blackPawn;

        public Sprite GetPieceSprite(int piece)
        {
            switch (piece)
            {
                case Piece.White + Piece.King: return whiteKing;
                case Piece.White + Piece.Queen: return whiteQueen;
                case Piece.White + Piece.Rook: return whiteRook;
                case Piece.White + Piece.Bishop: return whiteBishop;
                case Piece.White + Piece.Knight: return whiteKnight;
                case Piece.White + Piece.Pawn: return whitePawn;

                case Piece.Black + Piece.King: return blackKing;
                case Piece.Black + Piece.Queen: return blackQueen;
                case Piece.Black + Piece.Rook: return blackRook;
                case Piece.Black + Piece.Bishop: return blackBishop;
                case Piece.Black + Piece.Knight: return blackKnight;
                case Piece.Black + Piece.Pawn: return blackPawn;
                default: return null;
            }
        }
    }
}
