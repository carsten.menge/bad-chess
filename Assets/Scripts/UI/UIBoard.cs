using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Chess;
using System;

namespace Assets.Scripts.UI
{
    public class UIBoard : MonoBehaviour
    {
        public Color lightSquaresColor;
        public Color darkSquaresColor;
        public PieceSpriteHandler pieceSpriteHandler;
        public List<SpriteRenderer> drawnPieces;
        Board board;

        public void InitializeBoard(Board board)
        {
            this.board = board;
            DrawBoard();
            DrawPieces();
        }

        public void UpdateBoard()
        {
            RemoveAllPieces();
            DrawPieces();
        }

        private void DrawPieces()
        {
            for (int i = 0; i < 64; i++)
            {
                if (board.Squares[i] > 0)
                {
                    DrawPiece(i % 8, i / 8, board.Squares[i]);
                }
            }
        }

        private void RemoveAllPieces()
        {
            foreach (var spriteRenderer in drawnPieces)
            {
                Destroy(spriteRenderer.gameObject);
            }
            drawnPieces.Clear();
        }

        private void DrawBoard()
        {
            for (int file = 0; file < 8; file++)
            {
                for (int rank = 0; rank < 8; rank++)
                {
                    Color colour = ((file + rank) % 2 != 0) ? lightSquaresColor : darkSquaresColor;
                    Vector2 pos = new Vector2(file, rank);

                    DrawSquare(pos, colour);
                }
            }
        }

        private void DrawSquare(Vector2 position, Color colour)
        {
            Transform square = GameObject.CreatePrimitive(PrimitiveType.Quad).transform;
            square.parent = this.transform;
            square.position = new Vector3(position.x, position.y, 0);
            square.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Color");
            square.GetComponent<Renderer>().material.color = colour;
        }

        private void DrawPiece(int file, int rank, int piece)
        {
            SpriteRenderer pieceRenderer = new GameObject().AddComponent<SpriteRenderer>();
            pieceRenderer.transform.parent = this.transform;
            pieceRenderer.transform.position = new Vector3(file, rank, -0.1f);
            pieceRenderer.transform.localScale = Vector3.one * 100 / (2000 / 6f);
            pieceRenderer.sprite = pieceSpriteHandler.GetPieceSprite(piece);
            drawnPieces.Add(pieceRenderer);
        }
    }
}

